package com.tpandroid.countdowncounter

import android.os.CountDownTimer
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainActivityViewModel : ViewModel() {

    // Counter textView
    private val counterMutableLiveData = MutableLiveData<Long>(0)
    val counterLiveData : LiveData<Long> = counterMutableLiveData

    // isCountDownOver
    private var isCountDownOverMutableLiveData = MutableLiveData(false)
    val isCountDownOverLiveData : LiveData<Boolean> = isCountDownOverMutableLiveData

    // The given value by the user to start counting down from
    private var countDownStartValueMutableLiveData = MutableLiveData("")
    val countDownStartValueLiveData : LiveData<String> = countDownStartValueMutableLiveData

    // isCountDownPaused
    private var isCountDownPausedMutableLiveData = MutableLiveData(false)
    val isCountDownPausedLiveData : LiveData<Boolean> = isCountDownPausedMutableLiveData

    // Left time when the "Pause" Button is clicked
    private var leftTimeMutableLiveData : Long = 0

    private lateinit var timer : CountDownTimer



    fun setCountDownStartValue(value: String){
        countDownStartValueMutableLiveData.value = value
    }

    fun startTimer(){
        // convert to millis
        val millisInFuture = countDownStartValueMutableLiveData.value!!.toLong() * 1000

        timer = object : CountDownTimer(millisInFuture, 1000){

            // fired at each value of the interval "countDownInterval"
            override fun onTick(p0: Long) {
                counterMutableLiveData.value = p0/1000 // timeLeft convert millis to seconds
            }

            // fired at the end of the countdown
            override fun onFinish() {
                setCounterToZero()
            }

        }.start()
    }

    fun handlePauseResumeTimer(){

        if(isCountDownPausedMutableLiveData.value!!){
            leftTimeMutableLiveData = counterLiveData.value!!
            timer.cancel()
        }
        else {
            setCountDownStartValue(leftTimeMutableLiveData.toString())
            startTimer()
        }
    }

    fun stopTimer(){
        setCounterToZero()
        timer.cancel()
    }

    fun togglePauseResume(){
        isCountDownPausedMutableLiveData.value = isCountDownPausedMutableLiveData.value!!.not()
    }

    fun setCounterToZero(){
        isCountDownOverMutableLiveData.value = true
        counterMutableLiveData.value = 0
        leftTimeMutableLiveData = 0
        setCountDownStartValue("0")

    }


}