package com.tpandroid.countdowncounter

import android.graphics.Color
import android.os.Bundle
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.ViewModelProvider
import com.tpandroid.countdowncounter.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val mainActivityViewModel = ViewModelProvider(this)[MainActivityViewModel::class.java]

        // Handle the countdown TextView - Handle buttons visibility
        mainActivityViewModel.counterLiveData.observe(this) { counterValue ->
            binding.tvCounter.text = counterValue.toString()

            if(counterValue > 0 && counterValue < 5){
                binding.tvCounter.setTextColor(Color.RED)
            }

            if(counterValue == 0L){
                binding.etCountDownStartValue.setText("")
            }

            binding.btnStopCountDown.visibility = if(counterValue == 0L) GONE else VISIBLE
            binding.btnPauseContinue.visibility = if(counterValue == 0L) GONE else VISIBLE
            binding.btnStartCountDown.visibility = if(counterValue == 0L) VISIBLE else GONE
        }

        // Enable the "start" button if the given value is valid
        mainActivityViewModel.countDownStartValueLiveData.observe(this){ valueToStartCountFrom ->

            fun isValidCountStartValue() : Boolean {
                return valueToStartCountFrom.isNotEmpty() &&
                        valueToStartCountFrom.toLong() >= 1.toLong()
            }


            // TODO : Reset start value in order to give the user the possibility to restart the countdown
            //if(mainActivityViewModel.isCountDownOverLiveData.value!!){
            //    mainActivityViewModel.setCountDownStartValue(binding.etCountDownStartValue.text.toString())
            //}

            binding.btnStartCountDown.isEnabled = isValidCountStartValue()
        }

        // Handle the "Pause/Resume" text button
        mainActivityViewModel.isCountDownPausedLiveData.observe(this){ isCountDownPaused->
            binding.btnPauseContinue.text = if(isCountDownPaused) "resume" else "pause"
        }

        // display a Toast if the countdown is over
        mainActivityViewModel.isCountDownOverLiveData.observe(this){
            if(it){
                binding.tvCounter.setTextColor(Color.BLACK)
                Toast.makeText(this, "countdown over", Toast.LENGTH_LONG).show()
            }
        }

        binding.etCountDownStartValue.addTextChangedListener { countDownStartValue ->
            mainActivityViewModel.setCountDownStartValue(countDownStartValue.toString())
        }

        binding.btnStartCountDown.setOnClickListener{
            mainActivityViewModel.startTimer()

            // Hide the keyboard
            val imm: InputMethodManager =getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }

        binding.btnPauseContinue.setOnClickListener {
            mainActivityViewModel.togglePauseResume()
            mainActivityViewModel.handlePauseResumeTimer()
        }

        binding.btnStopCountDown.setOnClickListener{
            mainActivityViewModel.stopTimer()
        }
    }
}